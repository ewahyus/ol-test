package main

import (
	"fmt"
)

func main() {
	fmt.Println(">>>CONCURRENT")
	RunConcurrency()

	fmt.Println("")
	fmt.Println("")

	fmt.Println(">>>PARAREL")
	RunPararell()
}
