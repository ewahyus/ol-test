package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

func RunPararell() {
	runtime.GOMAXPROCS(4)
	var waitGroup sync.WaitGroup
	waitGroup.Add(5)

	fmt.Println("Starting goroutines")
	start := time.Now()

	go StepOne(&waitGroup)
	go StepTwo(&waitGroup)
	go StepThree(&waitGroup)
	go StepFour(&waitGroup)
	go StepFive(&waitGroup)

	fmt.Println("Waiting to finish..")
	waitGroup.Wait()

	fmt.Println("Termination Program")
	fmt.Println(time.Since(start))
}
