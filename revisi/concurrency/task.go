package main

import (
	"fmt"
	"sync"
)

func StepOne(waitgroup *sync.WaitGroup) {
	defer waitgroup.Done()
	fmt.Println("Boil the noodle for a short time")
}

func StepTwo(waitgroup *sync.WaitGroup) {
	defer waitgroup.Done()
	fmt.Println("Stir the noodle together with its seasoning and a couple of eggs")
}

func StepThree(waitgroup *sync.WaitGroup) {
	defer waitgroup.Done()
	fmt.Println("Heat the frying pan")
}

func StepFour(waitgroup *sync.WaitGroup) {
	defer waitgroup.Done()
	fmt.Println("Fry the stirred noodle")
}

func StepFive(waitgroup *sync.WaitGroup) {
	defer waitgroup.Done()
	fmt.Println("Serve the martabak mie on a plate")
}
