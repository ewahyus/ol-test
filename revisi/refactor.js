function setTitle(str) {
    let arr = [];
    arr = str.split(' ');
    let retour = arr.map(function(element){
        var tmp = [];
        tmp.push(element.charAt(0).toUpperCase());
        
        for(var i = 1; i < element.length; i++){
            tmp.push(element[i].toLowerCase());
        }

        return tmp.join('');

    });
    return retour.join(' ');
}
const title = "avengers end of game";
console.log(setTitle(title));