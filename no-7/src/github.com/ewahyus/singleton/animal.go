package main

import (
	"sync"
)

type animal struct {
	say string
}

var animalSay *animal
var once sync.Once

func GetAnimalSay() *animal {
	once.Do(func() {
		animalSay = &animal{say: "Coww"}
	})
	return animalSay
}
func (a *animal) GetState() string {
	return a.say
}
func (a *animal) SetState(s string) {
	a.say = s
}
