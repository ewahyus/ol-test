package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

type Superhero struct {
	fullname string `json:fullname`
	gender   string `json:gender`
	powers   string `json:powers`
	address  string `json:address`
	city     string `json:city`
	country  string `json:country`
}

type Response struct {
	status  int    `json:status`
	message string `json:message`
	data    []Superhero
}

func Connect() *sql.DB {
	db, err := sql.Open("mysql", "root:ekowahyu1993@tcp(localhost:3306)/test")
	if err != nil {
		log.Fatal(err)
	}
	return db
}

func CreateSuperhero(w http.ResponseWriter, r *http.Request) {
	var response Response

	db := Connect()
	defer db.Close()

	name := r.FormValue("name")
	gender := r.FormValue("gender")
	powers := r.FormValue("powers")
	address := r.FormValue("address")
	city := r.FormValue("city")

	_, err := db.Exec("INSERT INTO superheroes (superheroes_name, gender, address, power_id, city_id) VALUES (?, ?, ?, ?, ?)",
		name, gender, address, powers, city)

	if err != nil {
		panic(err)
	}

	response.status = 1
	response.message = "Success create superheroes"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}

func GetSuperheroes(w http.ResponseWriter, r *http.Request) {
	var response Response
	var heroes []Superhero
	var superheroes Superhero

	db := Connect()
	defer db.Close()

	rows, err := db.Query("select s.superheroes_name,s.gender, p.power_name, s.address, c.city_name, co.country_name from superheroes s join powers p on s.power_id = p.power_id join city c on s.city_id = c.city_id join country co on c.country_id = co.country_id")

	if err != nil {
		panic(err)
	}

	for rows.Next() {
		if err := rows.Scan(&superheroes.fullname, &superheroes.gender, &superheroes.powers, &superheroes.address, &superheroes.city, &superheroes.country); err != nil {
			log.Fatal(err.Error())
		} else {
			heroes = append(heroes, superheroes)
		}
	}

	response.status = 1
	response.message = "Success"
	response.data = heroes

	fmt.Printf("%v+", heroes)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(heroes)

}

func DeleteSuperheroes(w http.ResponseWriter, r *http.Request) {

	var response Response

	db := Connect()
	defer db.Close()

	id := r.FormValue("id")

	_, err := db.Exec("delete from superheroes where superheroes_id = ?", id)

	if err != nil {
		panic(err)
	}

	response.status = 1
	response.message = "Success Delete Data"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

func UpdateSuperheroes(w http.ResponseWriter, r *http.Request) {
	var response Response

	db := Connect()
	defer db.Close()

	id := r.FormValue("id")

	name := r.FormValue("name")
	gender := r.FormValue("gender")
	powers := r.FormValue("powers")
	address := r.FormValue("address")
	city := r.FormValue("city")

	_, err := db.Exec("UPDATE superheroes SET superheroes_name = ?, gender = ?, address = ?, power_id = ?, city_id = ? WHERE superheroes_id = ?",
		name,
		gender,
		address,
		powers,
		city,
		id,
	)

	if err != nil {
		panic(err)
	}

	response.status = 1
	response.message = "Success Update Data"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}

func main() {
	routes := mux.NewRouter()
	routes.HandleFunc("/api/superhero/create", CreateSuperhero).Methods("POST")
	routes.HandleFunc("/api/superhero/getAll", GetSuperheroes).Methods("GET")
	routes.HandleFunc("/api/superhero/delete", DeleteSuperheroes).Methods("DELETE")
	routes.HandleFunc("/api/superhero/update", UpdateSuperheroes).Methods("PUT")
	http.Handle("/", routes)
	http.ListenAndServe(":8080", nil)
}
