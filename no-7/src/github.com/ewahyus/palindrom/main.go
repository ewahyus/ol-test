package main

import (
	"fmt"
	"strings"
)

func checkPalindrom(value string) bool {
	var isPolindrom bool = true
	value = strings.ToLower(value)
	strLength := len(value)
	var i int

	for i = 0; i < strLength; i++ {
		if value[i] != value[strLength-i-1] {
			isPolindrom = false
		}
	}

	return isPolindrom

}

func main() {
	print := checkPalindrom("sugus")
	if print {
		fmt.Println("this is palindrome")
	} else {
		fmt.Println("this is not palindrome")
	}
}
