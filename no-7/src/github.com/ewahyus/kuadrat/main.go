package main

import (
	"fmt"
)

func main() {
	hasil := pangkat(2, 10)
	fmt.Println(hasil)
}

func pangkat(x, n int) int {
	if n == 0 {
		return 1
	} else {
		return x * pangkat(x, n-1)
	}
}
