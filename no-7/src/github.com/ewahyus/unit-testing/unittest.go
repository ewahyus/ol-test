package unittest

import (
	"fmt"
)

func ParseFloat(v interface{}) (float64, error) {
	switch v.(type) {
	case nil:
		return 1, nil
	default:
		return -1, fmt.Errorf("error number %v (type %T)", v, v)
	}

}
