package unittest

import (
	"testing"
)

func TestParseFloat(t *testing.T) {
	t.Run("Should return nil input", func(t *testing.T) {
		var i interface{}
		convertNum, _ := ParseFloat(i)
		if convertNum != 1 {
			t.Fatalf("return must 1 instead of %v", convertNum)
		}
	})
}
