function getCondition (condition) {
    var conditionElement = {
      'foo': 'Do foo stuffs',
      'bar': 'Do bar stuffs',
      'stool': 'Do stool stuffs',
      'default': 'Do default stuffs'
    };
    return (conditionElement[condition] || conditionElement['default']);
}
  
  var result = getCondition('foo');
  console.log(result);