CREATE TABLE powers (
    power_id INT AUTO_INCREMENT PRIMARY KEY,
    power_name VARCHAR(100)
);

CREATE TABLE country (
    country_id INT AUTO_INCREMENT PRIMARY KEY,
    country_name VARCHAR(100)
);

CREATE TABLE city (
    city_id INT AUTO_INCREMENT PRIMARY KEY,
    country_id INT,
    city_name VARCHAR(100),
    FOREIGN KEY (country_id) REFERENCES country(country_id)
);

CREATE TABLE superheroes (
    superheroes_id INT AUTO_INCREMENT PRIMARY KEY,
    superheroes_name VARCHAR(100),
    gender VARCHAR(50),
    address TEXT,
    power_id INT,
    city_id INT,
    FOREIGN KEY (power_id) REFERENCES powers(power_id),
    FOREIGN KEY (city_id) REFERENCES city(city_id)
);

