package main

import (
	"fmt"
	"sort"
)

type Superhero struct {
	name     string
	alterEgo string
	gender   string
	powers   []string
}

type GroupHero struct {
	gender string
	heroes Heroes
}

type Heroes struct {
	name     string
	alterEgo string
	powers   []string
}

func findDuplicateValue(powers []string) []string {
	countered := map[string]bool{}
	listPowers := []string{}

	for v := range powers {
		countered[powers[v]] = true
	}

	for keys, _ := range countered {
		listPowers = append(listPowers, keys)
	}

	return listPowers

}

func main() {
	dataHero := []Superhero{
		{
			name:     "Clark Ken",
			alterEgo: "Superman",
			gender:   "Male",
			powers:   []string{"super human strength", "flight", "x-ray vision", "heat vision"},
		},
		{
			name:     "Barry Allen",
			alterEgo: "The Flash",
			gender:   "Male",
			powers:   []string{"super speed", "phasing", "time travel", "dimensional travel"},
		},
		{
			name:     "Diana Prince",
			alterEgo: "Wonder Woman",
			gender:   "Female",
			powers:   []string{"super human strength", "super human reflexes", "super human agility", "flight"},
		},
		{
			name:     "Bruce Banner",
			alterEgo: "The Hulk",
			gender:   "Male",
			powers:   []string{"super human strength", "thunder clap", "super healing factor"},
		},
		{
			name:     "Wade Wilson",
			alterEgo: "Deadpool",
			gender:   "Male",
			powers:   []string{"super healing factor", "super human agility"},
		},
		{
			name:     "Jean Grey",
			alterEgo: "Phoenix",
			gender:   "Female",
			powers:   []string{"telepathy", "telekinesis", "rearrange matter at will"},
		},
		{
			name:     "Wanda Maximoff",
			alterEgo: "Scarlet Witch",
			gender:   "Female",
			powers:   []string{"reality manipulation", "astral projection", "psionic"},
		},
	}

	alterEgo := []string{}
	powers := []string{}
	listMale := []Superhero{}

	for _, value := range dataHero {
		alterEgo = append(alterEgo, value.alterEgo)
		if value.gender == "Male" {
			listMale = append(listMale, value)
		}
		for _, val := range value.powers {
			powers = append(powers, val)
		}
	}

	fmt.Println("-----------------Take only the alterEgo---------------------\n")
	fmt.Println(alterEgo)

	fmt.Println("-----------------Take only the powers---------------------\n")
	//remove duplicate powers
	listPowers := findDuplicateValue(powers)
	sort.Strings(listPowers)
	fmt.Println(listPowers)

	fmt.Println("-------------------Take only Male-------------------------\n")
	fmt.Printf("%+v\n", listMale)

}
